package ge.example.mycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.widget.TextView

class MainActivity() : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0

    private var operation: String = ""




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)

    }
    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result: String = resultTextView.text.toString()
            val number: String = clickedView.text.toString()
            if (result == "0")
                result = ""

            resultTextView.text = result + number
        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            val result: String = resultTextView.text.toString()
            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }
            operation = clickedView.text.toString()
            resultTextView.text = ""
        }
    }
    fun equalsClick(clickedView: View) {
        val secOperandText = resultTextView.text.toString()
        var secOperand = 0.0
        if (secOperandText.isNotEmpty()) {
            secOperand = secOperandText.toDouble()
        }
        when (operation) {
            "/" -> resultTextView.text = (operand / secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "%" -> resultTextView.text = ((operand * secOperand) / 100).toString()
            "+/-" -> resultTextView.text = (operand * (-1)).toString()
        }
    }
    fun clearView(clickedView: View) {
        resultTextView.text = ""

        operation = ""
        operand = 0.0
    }
}




